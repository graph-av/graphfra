resource "aws_instance" "monitor" {
  instance_type          = var.monitor_instance_types[terraform.workspace]
  ami                    = data.aws_ami.ubuntu.id
  key_name               = aws_key_pair.auth.id
  vpc_security_group_ids = [aws_security_group.monitor.id]
  subnet_id              = aws_subnet.public.0.id
  iam_instance_profile   = aws_iam_instance_profile.profile.name
  timeouts {
    create = "30m"
    delete = "10m"
  }
  user_data = templatefile("${abspath(path.root)}/monitor-cloud-init.yml", {
    fqdn              = "${var.prefix}-monitor-${terraform.workspace}.${var.root_domain}"
    gf_admin_password = var.gf_admin_password
    prefix            = var.prefix
    postgres_db       = var.postgres_db
    postgres_username = var.postgres_username
    postgres_password = var.postgres_password
    postgres_port     = var.postgres_port
    region            = var.workspace_regions[terraform.workspace]
    ssh_port          = var.ssh_port
    ssh_key_1         = var.ssh_key_1
    ssh_key_2         = var.ssh_key_2
    workspace         = terraform.workspace
  })
  connection {
    type        = "ssh"
    user        = var.prefix
    port        = var.ssh_port
    host        = self.public_ip
    private_key = file(var.private_key_path)
    agent       = false
  }
  root_block_device {
    volume_size = var.monitor_root_volume_size
  }
  ebs_block_device {
    device_name = "/dev/sdf"
    volume_size = var.monitor_data_volume_size
    volume_type = "gp2"
  }
  tags = {
    Name        = "${var.prefix}-monitor-${terraform.workspace}"
    environment = terraform.workspace
    group       = var.prefix
    type        = "monitor"
  }
  volume_tags = {
    Name        = "${var.prefix}-monitor-${terraform.workspace}"
    environment = terraform.workspace
    group       = var.prefix
    type        = "monitor"
  }
  provisioner "file" {
    source      = "../config/${terraform.workspace}.env"
    destination = "/home/${var.prefix}/.env"
  }
  provisioner "file" {
    source      = "../monitor"
    destination = "/home/${var.prefix}/monitor"
  }
  provisioner "file" {
    source      = "../utilities"
    destination = "/home/${var.prefix}/utilities"
  }
  provisioner "remote-exec" {
    inline = ["cloud-init status --wait",
      <<EOF
        find ~ -name '*.sh' | xargs  chmod +x
				/home/${var.prefix}/monitor/init.sh
      EOF
    ]
  }
}
