resource "aws_security_group" "indexer_service" {
  name        = "${var.prefix}-indexer-service-${terraform.workspace}"
  description = "${var.prefix} indexer service ${terraform.workspace}"
  vpc_id      = aws_vpc.main.id
  ingress {
    description = "all"
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = var.ip_whitelist
  }
  ingress {
    description     = "${var.prefix} node exporter"
    from_port       = 9100
    to_port         = 9100
    protocol        = "tcp"
    security_groups = [aws_security_group.monitor.id]
  }
  ingress {
    description     = "${var.prefix} indexer-service prometheus"
    from_port       = 7300
    to_port         = 7300
    protocol        = "tcp"
    security_groups = [aws_security_group.monitor.id]
  }
  ingress {
    description = "${var.prefix} indexer-service GraphQL http"
    from_port   = 7600
    to_port     = 7600
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }
  egress {
    description = "outbound internet access"
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }
  tags = {
    environment = terraform.workspace
    group       = var.prefix
    type        = "indexer-service"
  }
}
