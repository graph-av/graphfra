

resource "aws_instance" "indexer_agent" {
  ami                    = data.aws_ami.ubuntu.id
  count                  = var.indexer_agent_count[terraform.workspace]
  iam_instance_profile   = aws_iam_instance_profile.profile.name
  instance_type          = var.indexer_agent_instance_types[terraform.workspace]
  key_name               = aws_key_pair.auth.id
  subnet_id              = aws_subnet.public.0.id
  vpc_security_group_ids = [aws_security_group.indexer_agent.id]
  timeouts {
    create = "30m"
    delete = "10m"
  }
  user_data = templatefile("${abspath(path.root)}/indexer-agent-cloud-init.yml", {
    fqdn                 = "${var.prefix}-indexer-agent-${terraform.workspace}-${count.index + 1}.${var.root_domain}"
    indexer_agent_number = "${count.index + 1}"
    graph_node           = aws_instance.graph_node[count.index].private_ip
    prefix               = var.prefix
    postgres_db          = var.postgres_db
    postgres_username    = var.postgres_username
    postgres_password    = var.postgres_password
    postgres_port        = var.postgres_port
    region               = var.workspace_regions[terraform.workspace]
    ssh_port             = var.ssh_port
    ssh_key_1            = var.ssh_key_1
    ssh_key_2            = var.ssh_key_2
    workspace            = terraform.workspace
  })
  connection {
    type        = "ssh"
    user        = var.prefix
    port        = var.ssh_port
    host        = self.public_ip
    private_key = file(var.private_key_path)
    agent       = false
  }
  root_block_device {
    volume_size = var.indexer_agent_root_volume_size
  }
  tags = {
    Name        = "${var.prefix}-indexer-agent-${terraform.workspace}-${count.index + 1}"
    environment = terraform.workspace
    group       = var.prefix
    type        = "indexer-agent"
  }
  volume_tags = {
    Name        = "${var.prefix}-indexer-agent-${terraform.workspace}-${count.index + 1}"
    environment = terraform.workspace
    group       = var.prefix
    type        = "indexer-agent"
  }
  provisioner "file" {
    source      = "../indexer-agent"
    destination = "/home/${var.prefix}/"
  }
  provisioner "file" {
    source      = "../config/indexer-agent/${terraform.workspace}.env"
    destination = "/home/${var.prefix}/indexer-agent/indexer-agent.env"
  }
  provisioner "file" {
    source      = "../config/${terraform.workspace}.env"
    destination = "/home/${var.prefix}/indexer-agent/.env"
  }
  provisioner "file" {
    source      = "../utilities"
    destination = "/home/${var.prefix}/utilities"
  }
  provisioner "remote-exec" {
    inline = ["cloud-init status --wait",
      <<EOF
				find ~ -name '*.sh' | xargs chmod +x
				#echo "bash /home/${var.prefix}/indexer-agent/init.sh" | at now + 5 minutes
      EOF
    ]
  }
}
