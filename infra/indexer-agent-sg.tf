resource "aws_security_group" "indexer_agent" {
  name        = "${var.prefix}-indexer-agent-${terraform.workspace}"
  description = "${var.prefix} indexer agent ${terraform.workspace}"
  vpc_id      = aws_vpc.main.id
  ingress {
    description = "all"
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = var.ip_whitelist
  }
  ingress {
    description     = "${var.prefix} node exporter"
    from_port       = 9100
    to_port         = 9100
    protocol        = "tcp"
    security_groups = [aws_security_group.monitor.id]
  }
  egress {
    description = "outbound internet access"
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }
  tags = {
    environment = terraform.workspace
    group       = var.prefix
    type        = "indexer-agent"
  }
}
