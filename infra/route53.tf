data "aws_route53_zone" "root" {
  name         = var.root_domain
  private_zone = false
}

resource "aws_route53_record" "graph_node" {
  zone_id = data.aws_route53_zone.root.zone_id
  count   = var.graph_node_count[terraform.workspace]
  name    = "${var.prefix}.${var.root_domain}"
  type    = "A"
  alias {
    name                   = aws_elb.graphql[count.index].dns_name
    zone_id                = aws_elb.graphql[count.index].zone_id
    evaluate_target_health = true
  }
}

resource "aws_route53_record" "indexer" {
  zone_id = data.aws_route53_zone.root.zone_id
  count   = var.graph_node_count[terraform.workspace]
  name    = "${var.prefix}-public.${var.root_domain}"
  type    = "A"
  alias {
    name                   = aws_elb.graphql_paid[count.index].dns_name
    zone_id                = aws_elb.graphql_paid[count.index].zone_id
    evaluate_target_health = true
  }
}

resource "aws_route53_record" "monitor" {
  zone_id = data.aws_route53_zone.root.zone_id
  name    = "${var.prefix}-${var.workspace_regions[terraform.workspace]}-monitor.${var.root_domain}"
  type    = "A"
  alias {
    name                   = aws_elb.monitor.dns_name
    zone_id                = aws_elb.monitor.zone_id
    evaluate_target_health = true
  }
}

resource "aws_route53_record" "prometheus" {
  zone_id = data.aws_route53_zone.root.zone_id
  name    = "${var.prefix}-${var.workspace_regions[terraform.workspace]}-prometheus.${var.root_domain}"
  type    = "A"
  alias {
    name                   = aws_elb.prometheus.dns_name
    zone_id                = aws_elb.prometheus.zone_id
    evaluate_target_health = true
  }
}
