resource "aws_security_group" "graph_node" {
  name        = "${var.prefix}-graph-node-${terraform.workspace}"
  description = "${var.prefix} graph node ${terraform.workspace}"
  vpc_id      = aws_vpc.main.id
  ingress {
    description = "all"
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = var.ip_whitelist
  }
  ingress {
    description = "${var.prefix} GraphQL http"
    from_port   = 8000
    to_port     = 8000
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }
  ingress {
    description = "${var.prefix} GraphQL ws"
    from_port   = 8001
    to_port     = 8001
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }
  ingress {
    description     = "${var.prefix} admin api"
    from_port       = 8020
    to_port         = 8020
    protocol        = "tcp"
    security_groups = [aws_security_group.indexer_agent.id]
  }
  ingress {
    description     = "${var.prefix} indexing status"
    from_port       = 8030
    to_port         = 8030
    protocol        = "tcp"
    security_groups = [aws_security_group.monitor.id, aws_security_group.indexer_agent.id, aws_security_group.indexer_service.id]
  }
  ingress {
    description = "${var.prefix} prometheus"
    from_port   = 8040
    to_port     = 8040
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }
  ingress {
    description     = "${var.prefix} node exporter"
    from_port       = 9100
    to_port         = 9100
    protocol        = "tcp"
    security_groups = [aws_security_group.monitor.id]
  }
  egress {
    description = "outbound internet access"
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }
  tags = {
    environment = terraform.workspace
    group       = var.prefix
    type        = "graph-node"
  }
}
