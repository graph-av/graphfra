variable "workspace_regions" {
  default = {
    default  = "us-west-2"
    phaseone = "us-west-1"
    phasetwo = "us-west-1"
    stage    = "eu-west-1"
    prod     = "us-east-1"
  }
}

variable "workspace_azs" {
  default = {
    default  = ["us-west-2a", "us-west-2b", "us-west-2c"]
    phaseone = ["us-west-1a", "us-west-1b"]
    phasetwo = ["us-west-1a", "us-west-1b"]
    stage    = ["eu-west-1a", "eu-west-1b", "eu-west-1c"]
    prod     = ["us-east-1a", "us-east-1b", "us-east-1c", "us-east-1d", "us-east-1e", "us-east-1f", ]
  }
}

variable "graph_node_instance_types" {
  default = {
    default  = "t3a.medium"
    phaseone = "m5a.large"
    phasetwo = "m5a.large"
    stage    = "m5a.large"
    prod     = "m5a.large"
  }
}

variable "graph_node_count" {
  default = {
    default  = 1
    phaseone = 1
    phasetwo = 1
    stage    = 1
    prod     = 1
  }
}

variable "ipfs_node_instance_types" {
  default = {
    default  = "t3a.medium"
    phaseone = "t3a.medium"
    stage    = "t3a.large"
    prod     = "t3a.large"
  }
}

variable "ipfs_node_count" {
  default = {
    default  = 1
    phaseone = 1
    phasetwo = 1
    stage    = 1
    prod     = 1
  }
}

variable "db_instance_count" {
  default = {
    default  = 2
    phaseone = 2
    phasetwo = 2
    stage    = 3
    prod     = 4
  }
}

variable "monitor_instance_types" {
  default = {
    default  = "t3a.medium"
    phaseone = "t3a.medium"
    phasetwo = "t3a.medium"
    stage    = "t3a.large"
    prod     = "m5a.large"
  }
}

variable "indexer_agent_instance_types" {
  default = {
    default  = "t3a.medium"
    phaseone = "m5a.large"
    phasetwo = "m5a.large"
    stage    = "m5a.large"
    prod     = "m5a.large"
  }
}

variable "indexer_agent_count" {
  default = {
    default  = 1
    phaseone = 1
    phasetwo = 1
    stage    = 1
    prod     = 1
  }
}

variable "indexer_service_instance_types" {
  default = {
    default  = "t3a.medium"
    phaseone = "m5a.large"
    phasetwo = "m5a.large"
    stage    = "m5a.large"
    prod     = "m5a.large"
  }
}

variable "indexer_service_count" {
  default = {
    default  = 1
    phaseone = 1
    phasetwo = 1
    stage    = 1
    prod     = 1
  }
}

variable "prefix" {
  description = "Name of project being deployed for naming and tagging"
}

variable "ssh_port" {
  description = "sshd daemon listening port"
  default     = "22"
}

variable "public_key_path" {
  description = <<DESCRIPTION
Path to the SSH public key to be used for authentication.

Example: ~/.ssh/terraform.pub
DESCRIPTION

  default = "~/.ssh/id_rsa.pub"
}

variable "private_key_path" {
  description = <<DESCRIPTION
Path to the SSH private key to be used for authentication.

Example: ~/.ssh/id_rsa
DESCRIPTION

  default = "~/.ssh/id_rsa"
}

variable "graph_node_root_volume_size" {
  description = "Desired root volume size in GB"
  default     = "64"
}

variable "graph_node_data_volume_size" {
  description = "Desired chain data volume size in GB"
  default     = "128"
}

variable "ipfs_node_root_volume_size" {
  description = "Desired root volume size in GB"
  default     = "16"
}

variable "ipfs_node_data_volume_size" {
  description = "Desired chain data volume size in GB"
  default     = "16"
}

variable "monitor_root_volume_size" {
  description = "Desired root volume size in GB"
  default     = "16"
}

variable "monitor_data_volume_size" {
  description = "Desired chain data volume size in GB"
  default     = "16"
}

variable "indexer_agent_root_volume_size" {
  description = "Desired root volume size in GB"
  default     = "16"
}

variable "indexer_service_root_volume_size" {
  description = "Desired root volume size in GB"
  default     = "16"
}

variable "swap_size" {
  description = "Desired swap file size in GB"
  default     = "2"
}

variable "ubuntu_account_number" {
  description = "AMI owner ID"
  default     = "099720109477"
}

variable "indexer_snapshot_name" {
  description = "Graph indexer data snapshot name"
  default     = "indexer_data"
}

variable "ipfs_snapshot_name" {
  description = "IPFS data snapshot name"
  default     = "ipfs_data"
}

variable "root_domain" {
  description = "DNS root domain lookup"
}

variable "ip_whitelist" {
  description = "List of ip/cidr to be whitelisted for each ec2 instance"
}

variable "ssh_key_1" {
  description = "ssh key to add to allowed_hosts"
}

variable "ssh_key_2" {
  description = "ssh key to add to allowed_hosts"
}

variable "postgres_db" {
  description = "PostgreSQL db name to be used with graph node"
  default     = "graphnode"
}

variable "postgres_username" {
  description = "PostgreSQL username to be used with graph node"
}

variable "postgres_password" {
  description = "PostgreSQL password to be used with graph node"
}

variable "postgres_port" {
  description = "PostgreSQL port to be used with graph node"
  default     = 5432
}

variable "ipfs_path" {
  description = "IPFS path for data storage"
  default     = "/data/thegraph"
}

variable "gf_admin_password" {
  description = "Grafana admin password"
}
