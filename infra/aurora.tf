resource "aws_db_subnet_group" "main" {
  name       = "${var.prefix}-aurora-subnet-main-${terraform.workspace}"
  subnet_ids = aws_subnet.public.*.id

  tags = {
    Name        = "${var.prefix}-aurora-subnet-main-${terraform.workspace}"
    environment = terraform.workspace
    group       = var.prefix
    type        = "graph-db-aurora"
  }
}

resource "aws_rds_cluster" "postgresql" {
  apply_immediately                   = true
  cluster_identifier                  = "${var.prefix}-graph-db-aurora-${terraform.workspace}"
  engine                              = "aurora-postgresql"
  availability_zones                  = data.aws_availability_zones.available.names
  database_name                       = var.postgres_db
  master_username                     = var.postgres_username
  master_password                     = var.postgres_password
  port                                = var.postgres_port
  db_subnet_group_name                = aws_db_subnet_group.main.name
  vpc_security_group_ids              = [aws_security_group.postgresql.id]
  skip_final_snapshot                 = true
  preferred_backup_window             = "20:00-23:00"
  preferred_maintenance_window        = "sun:18:00-sun:19:00"
  backup_retention_period             = 5
  copy_tags_to_snapshot               = true
  enabled_cloudwatch_logs_exports     = ["postgresql"]
  iam_database_authentication_enabled = true
  tags = {
    Name        = "${var.prefix}-graph-db-aurora-${terraform.workspace}"
    environment = terraform.workspace
    group       = var.prefix
    type        = "graph-db-aurora"
  }
}

resource "aws_rds_cluster_instance" "cluster_instances" {
  count              = var.db_instance_count[terraform.workspace]
  identifier         = "${var.prefix}-aurora-${terraform.workspace}-${count.index + 1}"
  cluster_identifier = aws_rds_cluster.postgresql.id
  instance_class     = "db.r4.large"
  engine             = aws_rds_cluster.postgresql.engine
  engine_version     = aws_rds_cluster.postgresql.engine_version
}

resource "aws_security_group" "postgresql" {
  name        = "${var.prefix}-postgresql-${terraform.workspace}"
  description = "${var.prefix} postgresql ${terraform.workspace}"
  vpc_id      = aws_vpc.main.id
  ingress {
    description     = "postgres"
    from_port       = 5432
    to_port         = 5432
    protocol        = "tcp"
    cidr_blocks     = var.ip_whitelist
    security_groups = [aws_security_group.graph_node.id, aws_security_group.monitor.id, aws_security_group.indexer_agent.id, aws_security_group.indexer_service.id]
  }
  tags = {
    Name        = "${var.prefix}-postgresql-${terraform.workspace}"
    environment = terraform.workspace
    group       = var.prefix
    type        = "graph-db-aurora"
  }
}
