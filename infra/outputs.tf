output "graph-node-ip" {
  value = aws_instance.graph_node.*.public_ip
}
output "indexer-agent-ip" {
  value = aws_instance.indexer_agent.*.public_ip
}
output "indexer-service-ip" {
  value = aws_instance.indexer_service.*.public_ip
}
output "ipfs-node-ip" {
  value = aws_instance.ipfs_node.*.public_ip
}
output "monitor-ip" {
  value = aws_instance.monitor.public_ip
}
output "monitor-url" {
  value = aws_route53_record.monitor.name
}
output "prometheus-url" {
  value = aws_route53_record.prometheus.name
}
output "prefix" {
  value = var.prefix
}
output "env" {
  value = terraform.workspace
}
