#!/bin/bash

SCRIPT_PATH=$(dirname $(realpath -s $0))
cd ${SCRIPT_PATH}
set -o allexport
source .env
source indexer-agent.env
source /etc/environment
source ../utilities/rainbow.sh
source ../utilities/get-aurora-endpoint.sh
POSTGRES_ENDPOINT=$(get_aurora_endpoint)
export NVM_DIR="/home/${USER}/.nvm"
[ -s "${NVM_DIR}/nvm.sh" ] && \. "${NVM_DIR}/nvm.sh"
set +o allexport

graph-indexer-agent start \
	--graph-node-query-endpoint http://${GRAPH_NODE}:8000/ \
	--graph-node-admin-endpoint http://${GRAPH_NODE}:8020/ \
	--graph-node-status-endpoint http://${GRAPH_NODE}:8030/graphql \
	--public-indexer-url ${INDEXER_AGENT_PUBLIC_INDEXER_URL} \
	--indexer-management-port ${INDEXER_AGENT_INDEXER_MANAGEMENT_PORT} \
	--indexer-geo-coordinates ${INDEXER_AGENT_INDEXER_GEO_COORDINATES} \
	--postgres-host ${POSTGRES_ENDPOINT} \
	--postgres-port ${POSTGRES_PORT} \
	--postgres-database ${INDEXER_AGENT_POSTGRES_DATABASE} \
	--postgres-username ${POSTGRES_USERNAME} \
	--postgres-password ${POSTGRES_PASSWORD} \
	--index-node-ids default \
	--network-subgraph-endpoint ${INDEXER_AGENT_NETWORK_SUBGRAPH_ENDPOINT} \
	--ethereum ${INDEXER_AGENT_ETHEREUM} \
	--mnemonic "${INDEXER_AGENT_MNEMONIC}"
