#!/bin/bash

SCRIPT_PATH=$(dirname $(realpath -s $0))
cd ${SCRIPT_PATH}
set -o allexport
source .env
source indexer-agent.env
source ../utilities/rainbow.sh
source ../utilities/get-aurora-endpoint.sh
POSTGRES_ENDPOINT=$(get_aurora_endpoint)
PGPASSWORD=${POSTGRES_PASSWORD}
set +o allexport

~/utilities/install-cli.sh

createdb -h ${POSTGRES_ENDPOINT} -p ${POSTGRES_PORT} -U ${POSTGRES_USERNAME} -e ${INDEXER_AGENT_POSTGRES_DATABASE}

sudo cp ./indexer-agent.service /etc/systemd/system/ &&
	sudo systemctl enable indexer-agent &&
	sudo systemctl start indexer-agent
journalctl -fu indexer-agent