#!/bin/bash

sudo adduser --disabled-password --gecos "" ${1}
sudo usermod -aG sudo ${1}
echo "${1} ALL=(ALL) NOPASSWD: ALL" | sudo tee -a /etc/sudoers
sudo mkdir -p /home/${1}/.ssh
sudo cp ~/.ssh/authorized_keys /home/${1}/.ssh/authorized_keys
sudo touch /home/${1}/.cloud-warnings.skip
sudo chown -R ${1}:${1} /home/${1} 