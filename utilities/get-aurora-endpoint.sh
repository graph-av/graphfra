function get_aurora_endpoint() {
	echo $(aws rds describe-db-cluster-endpoints \
		--region ${REGION} |
		jq -r --arg IDENT thegraph-graph-db-aurora-${WORKSPACE} \
			'.DBClusterEndpoints[] 
| select (.DBClusterIdentifier==$IDENT) 
| select (.EndpointType=="WRITER") 
| .Endpoint')
}
