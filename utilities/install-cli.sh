#!/bin/bash

curl -o- https://raw.githubusercontent.com/creationix/nvm/v0.33.0/install.sh | bash
export NVM_DIR="/home/${USER}/.nvm"
[ -s "${NVM_DIR}/nvm.sh" ] && \. "${NVM_DIR}/nvm.sh"
nvm install 14
npm install -g npm-cli-login
npm-cli-login

npm install -g \
	--registry https://testnet.thegraph.com/npm-registry/ \
	@graphprotocol/graph-cli@0.19.0-alpha.0 \
	@graphprotocol/indexer-agent \
	@graphprotocol/indexer-cli \
	@graphprotocol/indexer-service
