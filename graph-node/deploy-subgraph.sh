#!/bin/bash

SCRIPT_PATH=$(dirname $(realpath -s $0))
cd ${SCRIPT_PATH}
set -o allexport
source .env
source graph-node.env
source ../utilities/rainbow.sh
set +o allexport

if [ $# != 3 ]; then
	echo "usage: ${0} <name> <ipfs_hash> <node>"
	exit 1
fi

DATA=$(printf '{"jsonrpc": "2.0", "method": "subgraph_create", "params": {"name":"%s"}, "id":"1"}' "${1}")
curl -H "content-type: application/json" --data "${DATA}" "${API}"

echo "Deploying ${1} (deployment ${2})"
DATA=$(printf '{"jsonrpc": "2.0", "method": "subgraph_deploy", "params": {"name":"%s", "ipfs_hash":"%s", "node_id":"%s"}, "id":"1"}' "${1}" "${2}" "${3}")

curl -H "content-type: application/json" --data "${DATA}" "${API}"
