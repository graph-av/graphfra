#!/bin/bash

SCRIPT_PATH=$(dirname $(realpath -s $0))
cd ${SCRIPT_PATH}
set -o allexport
source .env
source graph-node.env
source /etc/environment
source ../utilities/rainbow.sh
source ../utilities/get-aurora-endpoint.sh
source /home/thegraph/.cargo/env
POSTGRES_ENDPOINT=$(get_aurora_endpoint)
set +o allexport

cd graph-node &&
	cargo run -p graph-node --release -- \
		--postgres-url postgresql://${POSTGRES_USERNAME}:${POSTGRES_PASSWORD}@${POSTGRES_ENDPOINT}:${POSTGRES_PORT}/${POSTGRES_DB} \
		--ethereum-rpc mainnet:${MAINNET_ARCHIVE_NODE} \
		--ipfs ${IPFS_NODE}
#--node-id index_node_$[NODE_NUMBER-1]
