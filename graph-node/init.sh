#!/bin/bash

SCRIPT_PATH=$(dirname $(realpath -s $0))
cd ${SCRIPT_PATH}
set -o allexport
source .env
source graph-node.env
source ../utilities/rainbow.sh
source ../utilities/get-aurora-endpoint.sh
POSTGRES_ENDPOINT=$(get_aurora_endpoint)
PGPASSWORD=${POSTGRES_PASSWORD}
set +o allexport

~/utilities/create-data-volume.sh
~/utilities/install-cli.sh

curl https://sh.rustup.rs -sSf | sh -s -- -y &&
	source $HOME/.cargo/env

git clone https://github.com/graphprotocol/graph-node &&
	cd graph-node &&
	cargo build --release

echocyan "Starting graph node systemd service..."
cd ${SCRIPT_PATH} &&
	sudo cp ./graph-node.service /etc/systemd/system/ &&
	sudo systemctl enable graph-node &&
	sudo systemctl start graph-node
echogreen "Done!"

echocyan "Waiting for graph node to start..."
while ! echo exit | nc localhost 8020; do sleep 10; done
echogreen "Done!"
journalctl -fu graph-node
# ./deploy-subgraph.sh "molochventures/moloch" "QmTXzATwNfgGVukV1fX2T6xw9f6LAYRVWpsdXyRWzUR2H9" "index_node_0"
# ./deploy-subgraph.sh "uniswap/uniswap-v2" "QmXKwSEMirgWVn41nRzkT3hpUBw29cp619Gx58XW6mPhZP" "index_node_0"
# ./deploy-subgraph.sh "synthetixio-team/synthetix" "Qme2hDXrkBpuXAYEuwGPAjr6zwiMZV4FHLLBa3BHzatBWx" "index_node_0"
# ./deploy-subgraph.sh "jannis/gravity" "QmbeDC4G8iPAUJ6tRBu99vwyYkaSiFwtXWKwwYkoNphV4X" "index_node_0"