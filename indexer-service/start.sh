#!/bin/bash

SCRIPT_PATH=$(dirname $(realpath -s $0))
cd ${SCRIPT_PATH}
set -o allexport
source .env
source indexer-service.env
source /etc/environment
source ../utilities/rainbow.sh
source ../utilities/get-aurora-endpoint.sh
source /home/thegraph/.cargo/env
POSTGRES_ENDPOINT=$(get_aurora_endpoint)
SERVER_HOST=${POSTGRES_ENDPOINT}
SERVER_PORT=${POSTGRES_PORT}
SERVER_DB_NAME=${INDEXER_SERVICE_POSTGRES_DATABASE}
SERVER_DB_USER=${POSTGRES_USERNAME}
SERVER_DB_PASSWORD=${POSTGRES_PASSWORD}
export NVM_DIR="/home/${USER}/.nvm"
[ -s "${NVM_DIR}/nvm.sh" ] && \. "${NVM_DIR}/nvm.sh"
set +o allexport

graph-indexer-service start \
	--port ${INDEXER_SERVICE_PORT} \
	--graph-node-query-endpoint http://${GRAPH_NODE}:8000/ \
	--graph-node-status-endpoint http://${GRAPH_NODE}:8030/graphql \
	--network-subgraph-endpoint ${INDEXER_AGENT_NETWORK_SUBGRAPH_ENDPOINT} \
	--ethereum ${INDEXER_AGENT_ETHEREUM} \
	--mnemonic "${INDEXER_AGENT_MNEMONIC}"
