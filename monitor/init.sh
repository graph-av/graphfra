#!/bin/bash

SCRIPT_PATH=$(dirname $(realpath -s $0))
cd ${SCRIPT_PATH}
set -o allexport
source ~/.env
source /etc/environment
source ../utilities/rainbow.sh
source ../utilities/get-aurora-endpoint.sh
POSTGRES_ENDPOINT="$(get_aurora_endpoint)"
set +o allexport

~/utilities/create-data-volume.sh &&
	mkdir -p /data/prometheus &&
	chmod -R ugo+rw /data/

sed -i "s/<region>/${REGION}/g" ./prometheus/prometheus.yml &&
	sed -i "s/<postgres_endpoint>/${POSTGRES_ENDPOINT}/g" ./grafana/provisioning/datasources/datasource.yml &&
	sed -i "s/<postgres_port>/${POSTGRES_PORT}/g" ./grafana/provisioning/datasources/datasource.yml &&
	sed -i "s/<postgres_db>/${POSTGRES_DB}/g" ./grafana/provisioning/datasources/datasource.yml &&
	sed -i "s/<postgres_username>/${POSTGRES_USERNAME}/g" ./grafana/provisioning/datasources/datasource.yml &&
	sed -i "s/<postgres_password>/${POSTGRES_PASSWORD}/g" ./grafana/provisioning/datasources/datasource.yml

HOSTNAME=${HOSTNAME} docker-compose up -d
